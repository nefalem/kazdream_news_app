from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404

from .models import NewsAppModel
from .serializers import NewsAppSerializer


class AllNewsView(APIView):

    def get(self, request):
        """Вывод всех новостей"""
        all_news = NewsAppModel.objects.all()
        serializer = NewsAppSerializer(all_news, many=True)
        return Response({'All news': serializer.data})

    def post(self, request):
        """С моей точки зрение более корректная обработка POST запроса,
         когда id передается в теле запроса а не в url"""

        if NewsAppModel.objects.filter(id=request.data['id']).exists():
            return Response({'success': 'False', 'Error': 'id: {id} is already exits'.format(id=request.data['id'])})

        serializer = NewsAppSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            instance = serializer.save()
            return Response({'success': 'True', 'news created id': instance.id}, status=201)


class SingleNewsAppView(APIView):

    def get(self, request, id):
        """Обработка GET запроса"""
        single_news = get_object_or_404(NewsAppModel, id=id)
        serializer = NewsAppSerializer(single_news)
        return Response(serializer.data)

    def post(self, request, id):
        """Обработка POST запроса"""
        data = request.data
        data['id'] = id

        if NewsAppModel.objects.filter(id=id).exists():
            return Response({'success': 'False', 'Error': f'id: {id} is already exits'})

        serializer = NewsAppSerializer(data=data)

        if serializer.is_valid(raise_exception=True):
            instance = serializer.save()
            return Response({'success': 'True',  'created news id': instance.id}, status=201)

    def put(self, request, id):
        """Обработка PUT запроса"""
        saved_news = get_object_or_404(NewsAppModel.objects.all(), id=id)

        serializer = NewsAppSerializer(instance=saved_news, data=request.data, partial=True)

        if serializer.is_valid(raise_exception=True):
            news_saved = serializer.save()
            return Response({'success': 'True', 'status': f'news with id: {news_saved.id} successfully updated'},
                            status=200)

    def delete(self, request, id):
        """Обработка DELETE запроса"""
        saved_news = get_object_or_404(NewsAppModel.objects.all(), id=id)
        saved_news.delete()
        return Response({'success': 'True', 'status': f'news with id: {id} successfully deleted'},
                        status=204)
