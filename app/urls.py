from .views import AllNewsView, SingleNewsAppView
from django.urls import path

urlpatterns = [
    path('news/', AllNewsView.as_view()),
    path('news/<int:id>', SingleNewsAppView.as_view()),
]
