from rest_framework import serializers
from .models import NewsAppModel


class NewsAppSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=False, required=True)
    author = serializers.CharField(required=False)
    news_text = serializers.CharField(required=False)

    def create(self, validated_data):
        news_info = NewsAppModel.objects.create(**validated_data)
        return news_info

    def update(self, instance, validated_data):
        instance.author = validated_data.get('author', instance.author)
        instance.news_text = validated_data.get('news_text', instance.news_text)
        instance.save()
        return instance