from django.db import models

# Create your models here.


class NewsAppModel(models.Model):
    id = models.IntegerField(primary_key=True, editable=False, unique=True)
    author = models.CharField(max_length=255, default='', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    news_text = models.TextField(default='', blank=True)
