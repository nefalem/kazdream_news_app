# Dockerfile
FROM python:3.7
RUN mkdir /kazdream_news_app
WORKDIR /kazdream_news_app
COPY requirements.txt* /kazdream_news_app/
RUN pip install -r requirements.txt
ADD kazdream_news_app /kazdream_news_app/
